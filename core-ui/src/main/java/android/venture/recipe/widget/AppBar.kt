@file:OptIn(ExperimentalMaterial3Api::class)

package android.venture.recipe.widget

import android.venture.recipe.theme.AppColor
import android.venture.recipe.theme.AppTypography
import androidx.annotation.DrawableRes
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.TopAppBarScrollBehavior
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow

@Composable
fun AppBar(
    title: String?,
    modifier: Modifier = Modifier,
    @DrawableRes navigationIcon: Int? = null,
    onClickNavigationIcon: (() -> Unit)? = null,
    scrollBehavior: TopAppBarScrollBehavior? = null,
    actions: @Composable RowScope.() -> Unit = {},
) {
    TopAppBar(
        modifier = modifier,
        title = {
            if (title != null) {
                Text(
                    text = title,
                    style = AppTypography.titleMediumBold,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                )
            }
        },
        navigationIcon = {
            if (navigationIcon != null) {
                Icon(
                    modifier = Modifier.clickable { onClickNavigationIcon?.invoke() },
                    painter = painterResource(id = navigationIcon),
                    tint = AppColor.mist10,
                    contentDescription = "navigation icon",
                )
            }
        },
        colors = TopAppBarDefaults.topAppBarColors(
            containerColor = if (title != null) MaterialTheme.colorScheme.primary else Color.Transparent,
            scrolledContainerColor = if (title != null) MaterialTheme.colorScheme.primary else Color.Transparent,
            navigationIconContentColor = AppColor.mist100,
            titleContentColor = AppColor.mist10,
            actionIconContentColor = AppColor.mist10,
        ),
        scrollBehavior = scrollBehavior,
        actions = actions,
    )
}