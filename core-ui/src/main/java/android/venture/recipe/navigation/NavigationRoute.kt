package android.venture.recipe.navigation

object RecipeListNavigationRoute {
    const val RECIPE_LIST_NAVGRAPH_ROUTE = "recipeListNavgraphRoute"
    const val RECIPE_LIST_ROUTE = "recipeListRoute"
    const val RECIPE_DETAIL_ROUTE = "recipeDetailRoute"
    const val RECIPE_DETAIL_ID_ARGS = "recipeDetailIdArgs"
}

object LoginNavigationRoute {
    const val LOGIN_NAVGRAPH_ROUTE = "loginNavgraphRoute"
    const val LANDING_ROUTE = "landingRoute"
    const val REGISTER_ROUTE = "registerRoute"
}