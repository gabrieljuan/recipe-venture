@file:OptIn(ExperimentalEncodingApi::class)

package android.venture.recipe.data.datasource

import android.content.SharedPreferences
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.constant.SecureStorageKey.LOGIN_TOKEN
import android.venture.recipe.data.repository.AuthRepository
import javax.inject.Inject
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

class AuthLocalDatasource @Inject constructor(
    private val sharedPreferences: SharedPreferences
) : AuthRepository {
    override suspend fun register(username: String, password: String): DataResult<Unit> {
        val loginToken = Base64.encode("$username:$password".toByteArray())
        return sharedPreferences.edit()
            .putString(username, password)
            .putString(LOGIN_TOKEN, loginToken)
            .commit().let { isSuccess ->
                if (isSuccess) {
                    DataResult.Success(Unit)
                } else {
                    DataResult.Exception(Throwable("Failed to save credentials"))
                }
            }
    }

    override suspend fun login(username: String, password: String): DataResult<Unit> {
        if (sharedPreferences.getString(username, null) != password) {
            return DataResult.Exception(Throwable("username or password is wrong"))
        }

        val loginToken = Base64.encode("$username:$password".toByteArray())
        return sharedPreferences.edit()
            .putString(LOGIN_TOKEN, loginToken)
            .commit().let { isSuccess ->
                if (isSuccess) {
                    DataResult.Success(Unit)
                } else {
                    DataResult.Exception(Throwable("Failed to save token"))
                }
            }
    }

    override suspend fun loginUsingToken(): DataResult<Unit> {
        val loginToken = sharedPreferences.getString(LOGIN_TOKEN, null)
            ?: return DataResult.Exception(Throwable("Token not found"))
        return Base64.decode(loginToken)
            .toString(Charsets.UTF_8)
            .split(':')
            .let {
                if (sharedPreferences.getString(it[0], null) == it[1]) {
                    DataResult.Success(Unit)
                } else {
                    DataResult.Exception(Throwable("Invalid token"))
                }
            }
    }

    override suspend fun logout(): DataResult<Unit> {
        return sharedPreferences.edit()
            .remove(LOGIN_TOKEN)
            .commit().let { isSuccess ->
                if (isSuccess) {
                    DataResult.Success(Unit)
                } else {
                    DataResult.Exception(Throwable("Failed to logout"))
                }
            }
    }

}