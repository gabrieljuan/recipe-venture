package android.venture.recipe.data.model

data class MealList(
    val meals: List<Meal>
)