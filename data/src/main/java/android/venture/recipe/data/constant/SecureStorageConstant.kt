package android.venture.recipe.data.constant

object SecureStorage{
    const val SECURE_STORAGE_FILE_NAME = "secure_shared_pref"
}

object SecureStorageKey {
    const val LOGIN_TOKEN = "login_token"
}