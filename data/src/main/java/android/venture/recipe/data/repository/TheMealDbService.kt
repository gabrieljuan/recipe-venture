package android.venture.recipe.data.repository

import android.venture.recipe.data.constant.TheMealDbApiQueryParam
import android.venture.recipe.data.constant.TheMealDbApiUrl
import android.venture.recipe.data.model.MealList
import retrofit2.http.GET
import retrofit2.http.Query

interface TheMealDbService {

    @GET(TheMealDbApiUrl.SEARCH)
    suspend fun searchByLetter(@Query(TheMealDbApiQueryParam.SEARCH_NAME_QUERY) letter: String): MealList

    @GET(TheMealDbApiUrl.DETAILS)
    suspend fun getMealDetail(@Query(TheMealDbApiQueryParam.DETAILS_ID_QUERY) id: String): MealList
}