/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.venture.recipe.data.di

import android.venture.recipe.data.datasource.AuthLocalDatasource
import android.venture.recipe.data.datasource.TheMealiDbNetworkDatasource
import android.venture.recipe.data.repository.AuthRepository
import android.venture.recipe.data.repository.TheMealDbRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Singleton
    @Binds
    fun bindsTheMealDbRepository(
        networkDatasource: TheMealiDbNetworkDatasource
    ): TheMealDbRepository

    @Singleton
    @Binds
    fun bindsLoginRepository(
        localDatasource: AuthLocalDatasource
    ): AuthRepository
}
