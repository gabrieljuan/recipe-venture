package android.venture.recipe.data.repository

import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.model.MealList

interface TheMealDbRepository {

    suspend fun searchByLetter(letter: Char): DataResult<MealList>

    suspend fun getMealDetail(id: String): DataResult<MealList>
}
