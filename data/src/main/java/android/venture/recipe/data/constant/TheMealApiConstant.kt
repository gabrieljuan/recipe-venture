package android.venture.recipe.data.constant

object TheMealDbApiUrl {
    const val BASE_URL = "https://www.themealdb.com/api/json/v1/1/"
    const val SEARCH = "search.php"
    const val DETAILS = "lookup.php"
}

object TheMealDbApiQueryParam {
    const val SEARCH_NAME_QUERY = "f"
    const val DETAILS_ID_QUERY = "i"
}
