package android.venture.recipe.data.repository

import android.venture.recipe.core.model.DataResult

interface AuthRepository {

    suspend fun register(username:String, password: String): DataResult<Unit>

    suspend fun login(username: String, password: String): DataResult<Unit>

    suspend fun loginUsingToken(): DataResult<Unit>

    suspend fun logout(): DataResult<Unit>
}