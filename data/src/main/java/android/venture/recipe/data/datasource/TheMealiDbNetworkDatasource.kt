package android.venture.recipe.data.datasource

import android.venture.recipe.data.model.MealList
import android.venture.recipe.data.repository.TheMealDbRepository
import android.venture.recipe.data.repository.TheMealDbService
import android.venture.recipe.data.util.tryGetDataCall
import android.venture.recipe.core.model.DataResult
import javax.inject.Inject

class TheMealiDbNetworkDatasource @Inject constructor(
    private val service: TheMealDbService,
) : TheMealDbRepository {

    override suspend fun searchByLetter(letter: Char): DataResult<MealList> = tryGetDataCall {
        service.searchByLetter(letter.toString())
    }

    override suspend fun getMealDetail(id: String): DataResult<MealList> = tryGetDataCall {
        service.getMealDetail(id)
    }
}
