package android.venture.recipe.data.util

import android.util.Log
import android.venture.recipe.core.model.DataResult
import retrofit2.Response

inline fun <T : Any> tryGetDataCall(dataCall: () -> T): DataResult<T> = try {
    val value = dataCall()
    DataResult.Success(value)
} catch (ex: Throwable) {
    ex.printStackTrace()
    DataResult.Exception(ex)
}
