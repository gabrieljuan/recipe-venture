@file:OptIn(ExperimentalEncodingApi::class)

package android.venture.recipe.data

import android.content.SharedPreferences
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.constant.SecureStorageKey
import android.venture.recipe.data.datasource.AuthLocalDatasource
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi

class AuthLocalDatasourceTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var sharedPreferences: SharedPreferences

    @Mock
    private lateinit var editor: SharedPreferences.Editor

    private lateinit var mock: AutoCloseable

    private lateinit var datasource: AuthLocalDatasource

    @Before
    fun setup() {
        mock = MockitoAnnotations.openMocks(this)
        Mockito.`when`(sharedPreferences.edit()).thenReturn(editor)
        datasource = AuthLocalDatasource(sharedPreferences)
    }

    @After
    fun teardown() {
        mock.close()
    }

    @Test
    fun `test register returns success`() = runTest {
        // Given
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(true)

        // When
        val result = datasource.register("test", "test")

        // Then
        Assert.assertTrue(result is DataResult.Success)
    }

    @Test
    fun `test register returns failed`() = runTest {
        // Given
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(false)

        // When
        val result = datasource.register("test", "test")

        // Then
        Assert.assertTrue(result is DataResult.Exception)
        Assert.assertEquals(
            "Failed to save credentials",
            (result as DataResult.Exception).throwable.message
        )
    }

    @Test
    fun `test login returns success`() = runTest {
        // Given
        Mockito.`when`(sharedPreferences.getString("test", null)).thenReturn("password")
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(true)

        // When
        val result = datasource.login("test", "password")

        // Then
        Assert.assertTrue(result is DataResult.Success)
    }

    @Test
    fun `test login returns failed when saving credentials`() = runTest {
        // Given
        Mockito.`when`(sharedPreferences.getString("test", null)).thenReturn("password")
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(false)

        // When
        val result = datasource.login("test", "password")

        // Then
        Assert.assertTrue(result is DataResult.Exception)
        Assert.assertEquals(
            "Failed to save token",
            (result as DataResult.Exception).throwable.message
        )
    }

    @Test
    fun `test login with wrong password returns failed`() = runTest {
        // Given
        Mockito.`when`(sharedPreferences.getString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn("password")
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(false)

        // When
        val result = datasource.login("test", "test")

        // Then
        Assert.assertTrue(result is DataResult.Exception)
        Assert.assertEquals(
            "username or password is wrong",
            (result as DataResult.Exception).throwable.message
        )
    }

    @Test
    fun `test login using token returns success`() = runTest {
        // Given
        val token = Base64.encode("test:password".toByteArray())
        Mockito.`when`(sharedPreferences.getString(SecureStorageKey.LOGIN_TOKEN, null))
            .thenReturn(token)
        Mockito.`when`(sharedPreferences.getString("test", null)).thenReturn("password")
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(true)

        // When
        val result = datasource.loginUsingToken()

        // Then
        Assert.assertTrue(result is DataResult.Success)
    }

    @Test
    fun `test login with token returns no token found failed`() = runTest {
        // Given
        Mockito.`when`(sharedPreferences.getString(SecureStorageKey.LOGIN_TOKEN, null))
            .thenReturn(null)
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(false)

        // When
        val result = datasource.loginUsingToken()

        // Then
        Assert.assertTrue(result is DataResult.Exception)
        Assert.assertEquals("Token not found", (result as DataResult.Exception).throwable.message)
    }

    @Test
    fun `test login using token returns invalid token failed`() = runTest {
        // Given
        val token = Base64.encode("test:test".toByteArray())
        Mockito.`when`(sharedPreferences.getString(SecureStorageKey.LOGIN_TOKEN, null))
            .thenReturn(token)
        Mockito.`when`(sharedPreferences.getString("test", null)).thenReturn("password")
        Mockito.`when`(editor.putString(Mockito.anyString(), Mockito.anyString()))
            .thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(true)

        // When
        val result = datasource.loginUsingToken()

        // Then
        Assert.assertTrue(result is DataResult.Exception)
        Assert.assertEquals("Invalid token", (result as DataResult.Exception).throwable.message)
    }

    @Test
    fun `test logout return success`() = runTest {
        // Given
        Mockito.`when`(editor.remove(SecureStorageKey.LOGIN_TOKEN)).thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(true)

        // When
        val result = datasource.logout()

        // Then
        Assert.assertTrue(result is DataResult.Success)
    }

    @Test
    fun `test logout return failed`() = runTest {
        // Given
        Mockito.`when`(editor.remove(SecureStorageKey.LOGIN_TOKEN)).thenReturn(editor)
        Mockito.`when`(editor.commit()).thenReturn(false)

        // When
        val result = datasource.logout()

        // Then
        Assert.assertTrue(result is DataResult.Exception)
        Assert.assertEquals("Failed to logout", (result as DataResult.Exception).throwable.message)
    }
}