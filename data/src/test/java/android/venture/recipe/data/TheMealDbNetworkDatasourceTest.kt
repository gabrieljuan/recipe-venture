package android.venture.recipe.data

import android.app.Application
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.datasource.TheMealiDbNetworkDatasource
import android.venture.recipe.data.model.MealList
import android.venture.recipe.data.repository.TheMealDbService
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class TheMealDbNetworkDatasourceTest {

    @get:Rule
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var retrofitService: TheMealDbService

    private lateinit var mock: AutoCloseable

    private lateinit var datasource: TheMealiDbNetworkDatasource

    @Before
    fun setup() {
        mock = MockitoAnnotations.openMocks(this)
        datasource = TheMealiDbNetworkDatasource(retrofitService)
    }

    @After
    fun teardown() {
        mock.close()
    }

    @Test
    fun `test searchByLetter`() = runTest {
        // Given
        val mealList = MealList(listOf())
        Mockito.`when`(retrofitService.searchByLetter(Mockito.anyString()))
            .thenReturn(mealList)

        // When
        val result = datasource.searchByLetter('A')

        // Then
        Assert.assertTrue(result is DataResult.Success)
        Assert.assertEquals(mealList, (result as DataResult.Success).value)
    }

    @Test
    fun `test getMealDetail`() = runTest {
        // Given
        val mealList = MealList(listOf())
        Mockito.`when`(retrofitService.getMealDetail(Mockito.anyString()))
            .thenReturn(mealList)

        // When
        val result = datasource.getMealDetail("A")

        // Then
        Assert.assertTrue(result is DataResult.Success)
        Assert.assertEquals(mealList, (result as DataResult.Success).value)
    }
}