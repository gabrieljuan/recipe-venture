Recipe Venture (Personal Project)
==================

This project should be run in latest Android Development Environment

## Screenshots

![Screenshot](https://gitlab.com/gabrieljuan/recipe-venture/-/raw/master/landing_page.png)
![Screenshot](https://gitlab.com/gabrieljuan/recipe-venture/-/raw/master/recipe_list.png)
![Screenshot](https://gitlab.com/gabrieljuan/recipe-venture/-/raw/master/recipe_detail.png)
![Screenshot](https://gitlab.com/gabrieljuan/recipe-venture/-/raw/master/zoom_feature.png)

## Features

* Hilt Dependency Injection
* MVVM / MVI
* UI in Compose
* Navigation
* Repository and data source
* Kotlin Coroutines and LiveData
* Retrofit
* Encrypted SharedPref for login and logout
* Base64 token for credentials
* Zoomable image (Recipe list by clicking the thumbnail and Recipe Detail)
* Unit test

## Screen
* Login / Landing Screen
* Register Screen
* Search meal by first letter
* Meal detail

# License

Now in Android is distributed under the terms of the Apache License (Version 2.0). See the
[license](LICENSE) for more information.
