package android.venture.recipe.domain.usecase

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseUseCase
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.model.Meal
import android.venture.recipe.data.model.MealList
import android.venture.recipe.data.repository.TheMealDbRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchMealByIdUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val repository: TheMealDbRepository,
) : BaseUseCase<FetchMealByIdUseCase.Params, MealList, FetchMealByIdActionResult>(
    coroutineDispatcher
) {

    data class Params(val id: String)

    override suspend fun execute(param: Params?): DataResult<MealList> {
        if (param == null) throw IllegalArgumentException("Param cannot be null")
        return repository.getMealDetail(param.id)
    }

    override suspend fun DataResult<MealList>.transformToUseCaseResult(): FetchMealByIdActionResult =
        when (this) {
            is DataResult.Success -> {
                if (value.meals.isNotEmpty())
                    FetchMealByIdActionResult.Success(value.meals.first())
                else
                    FetchMealByIdActionResult.Empty
            }

            is DataResult.Exception -> FetchMealByIdActionResult.Failed
        }
}


sealed interface FetchMealByIdActionResult : ActionResult {
    data class Success(val meal: Meal) : FetchMealByIdActionResult
    object Failed : FetchMealByIdActionResult
    object Empty : FetchMealByIdActionResult
}