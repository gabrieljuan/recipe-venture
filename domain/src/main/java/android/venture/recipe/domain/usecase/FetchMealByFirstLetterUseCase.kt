package android.venture.recipe.domain.usecase

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseUseCase
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.model.Meal
import android.venture.recipe.data.model.MealList
import android.venture.recipe.data.repository.TheMealDbRepository
import kotlinx.coroutines.CoroutineDispatcher

class FetchMealByFirstLetterUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val repository: TheMealDbRepository,
) : BaseUseCase<FetchMealByFirstLetterUseCase.Params, MealList, FetchMealByFirstLetterActionResult>(
    coroutineDispatcher
) {

    data class Params(val letter: Char)

    override suspend fun execute(param: Params?): DataResult<MealList> {
        if (param == null) throw IllegalArgumentException("Param cannot be null")
        return repository.searchByLetter(param.letter)
    }

    override suspend fun DataResult<MealList>.transformToUseCaseResult(): FetchMealByFirstLetterActionResult =
        when (this) {
            is DataResult.Success -> {
                if (value.meals.isNotEmpty())
                    FetchMealByFirstLetterActionResult.Success(value.meals)
                else
                    FetchMealByFirstLetterActionResult.Empty
            }

            is DataResult.Exception -> FetchMealByFirstLetterActionResult.Failed
        }
}

sealed interface FetchMealByFirstLetterActionResult : ActionResult {
    data class Success(val mealList: List<Meal>) : FetchMealByFirstLetterActionResult
    object Failed : FetchMealByFirstLetterActionResult
    object Empty : FetchMealByFirstLetterActionResult
}