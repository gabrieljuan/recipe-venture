package android.venture.recipe.domain.usecase

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseUseCase
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.repository.AuthRepository
import kotlinx.coroutines.CoroutineDispatcher

class LoginUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val authRepository: AuthRepository,
) : BaseUseCase<LoginUseCase.Params, Unit, LoginUseCaseActionResult>(coroutineDispatcher) {

    data class Params(val username: String, val password: String)

    override suspend fun execute(param: Params?): DataResult<Unit> =
        if (param == null) {
            authRepository.loginUsingToken()
        } else {
            authRepository.login(param.username, param.password)
        }


    override suspend fun DataResult<Unit>.transformToUseCaseResult(): LoginUseCaseActionResult =
        when (this) {
            is DataResult.Success -> LoginUseCaseActionResult.Success
            is DataResult.Exception -> LoginUseCaseActionResult.Failed(throwable)
        }
}

sealed interface LoginUseCaseActionResult : ActionResult {
    object Success : LoginUseCaseActionResult

    data class Failed(val throwable: Throwable) : LoginUseCaseActionResult
}