package android.venture.recipe.domain.usecase

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseUseCase
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.repository.AuthRepository
import kotlinx.coroutines.CoroutineDispatcher

class RegisterUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val authRepository: AuthRepository,
) : BaseUseCase<RegisterUseCase.Params, Unit, RegisterUseCaseActionResult>(coroutineDispatcher) {

    data class Params(val username: String, val password: String)

    override suspend fun execute(param: Params?): DataResult<Unit> {
        if (param == null) throw IllegalArgumentException("Param cannot be null")
        return authRepository.register(param.username, param.password)
    }

    override suspend fun DataResult<Unit>.transformToUseCaseResult(): RegisterUseCaseActionResult =
        when (this) {
            is DataResult.Success -> RegisterUseCaseActionResult.Success
            is DataResult.Exception -> RegisterUseCaseActionResult.Failed(throwable)
        }
}

sealed interface RegisterUseCaseActionResult : ActionResult {
    object Success : RegisterUseCaseActionResult
    data class Failed(val throwable: Throwable) : RegisterUseCaseActionResult
}