package android.venture.recipe.domain.usecase

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseUseCase
import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.repository.AuthRepository
import kotlinx.coroutines.CoroutineDispatcher

class LogoutUseCase(
    coroutineDispatcher: CoroutineDispatcher,
    private val authRepository: AuthRepository,
) : BaseUseCase<Nothing, Unit, LogoutUseActionResult>(coroutineDispatcher) {
    override suspend fun execute(param: Nothing?): DataResult<Unit> =
        authRepository.logout()

    override suspend fun DataResult<Unit>.transformToUseCaseResult(): LogoutUseActionResult =
        when (this) {
            is DataResult.Success -> LogoutUseActionResult.Success
            is DataResult.Exception -> LogoutUseActionResult.Failed(throwable)
        }

}

sealed interface LogoutUseActionResult : ActionResult {
    object Success : LogoutUseActionResult
    data class Failed(val throwable: Throwable) : LogoutUseActionResult
}