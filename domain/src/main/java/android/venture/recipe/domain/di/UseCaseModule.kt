package android.venture.recipe.domain.di

import android.venture.recipe.data.repository.AuthRepository
import android.venture.recipe.data.repository.TheMealDbRepository
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterUseCase
import android.venture.recipe.domain.usecase.FetchMealByIdUseCase
import android.venture.recipe.domain.usecase.LoginUseCase
import android.venture.recipe.domain.usecase.LogoutUseCase
import android.venture.recipe.domain.usecase.RegisterUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    fun provideCoroutine(): CoroutineDispatcher = Dispatchers.IO

    @Provides
    fun provideFetchMealByFirstLetterUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: TheMealDbRepository,
    ): FetchMealByFirstLetterUseCase =
        FetchMealByFirstLetterUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideFetchMealByIdUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: TheMealDbRepository,
    ): FetchMealByIdUseCase = FetchMealByIdUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideLoginUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: AuthRepository,
    ): LoginUseCase = LoginUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideLogoutUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: AuthRepository,
    ): LogoutUseCase = LogoutUseCase(coroutineDispatcher, repository)

    @Provides
    fun provideRegisterUseCase(
        coroutineDispatcher: CoroutineDispatcher,
        repository: AuthRepository,
    ): RegisterUseCase = RegisterUseCase(coroutineDispatcher, repository)
}