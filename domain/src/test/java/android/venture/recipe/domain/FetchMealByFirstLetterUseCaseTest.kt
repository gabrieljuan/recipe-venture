package android.venture.recipe.domain

import android.venture.recipe.core.model.DataResult
import android.venture.recipe.data.model.Meal
import android.venture.recipe.data.model.MealList
import android.venture.recipe.data.repository.TheMealDbRepository
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterActionResult
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class FetchMealByFirstLetterUseCaseTest {

    @Rule
    @JvmField
    val rule: MockitoRule = MockitoJUnit.rule()

    @Mock
    private lateinit var repository: TheMealDbRepository

    private lateinit var usecase: FetchMealByFirstLetterUseCase

    private lateinit var mock: AutoCloseable

    @Mock
    private lateinit var meal: Meal

    @Before
    fun setup(){
        mock = MockitoAnnotations.openMocks(this)
        usecase = FetchMealByFirstLetterUseCase(
            coroutineDispatcher = Dispatchers.IO,
            repository = repository,
        )
    }

    @After
    fun teardown() {
        mock.close()
    }

    @Test
    fun `test when call fetch meal by first letter return success`() = runTest {
        // Given
        Mockito.`when`(repository.searchByLetter(Mockito.anyChar()))
            .thenReturn(DataResult.Success(MealList(listOf(meal))))

        // When
        val result = usecase.getResult(FetchMealByFirstLetterUseCase.Params('A'))

        // Then
        Assert.assertTrue(result is FetchMealByFirstLetterActionResult.Success)
        Assert.assertTrue((result as FetchMealByFirstLetterActionResult.Success).mealList.size == 1)
    }

    @Test
    fun `test when call fetch meal by first letter return empty`() = runTest {
        // Given
        Mockito.`when`(repository.searchByLetter(Mockito.anyChar()))
            .thenReturn(DataResult.Success(MealList(listOf())))

        // When
        val result = usecase.getResult(FetchMealByFirstLetterUseCase.Params('A'))

        // Then
        Assert.assertTrue(result is FetchMealByFirstLetterActionResult.Empty)
    }

    @Test
    fun `test when call fetch meal by first letter return failed`() = runTest {
        // Given
        Mockito.`when`(repository.searchByLetter(Mockito.anyChar()))
            .thenReturn(DataResult.Exception(Throwable("error")))

        // When
        val result = usecase.getResult(FetchMealByFirstLetterUseCase.Params('A'))

        // Then
        Assert.assertTrue(result is FetchMealByFirstLetterActionResult.Failed)
    }
}