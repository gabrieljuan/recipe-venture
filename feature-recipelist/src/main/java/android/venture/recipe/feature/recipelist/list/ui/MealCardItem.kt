@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package android.venture.recipe.feature.recipelist.list.ui

import android.venture.recipe.feature.recipelist.R
import android.venture.recipe.theme.AppColor
import android.venture.recipe.theme.AppTypography
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage

@Composable
fun MealCardItem(
    mealName: String,
    mealCategory: String,
    mealOrigin: String,
    mealTags: String,
    mealThumbnailUrl: String,
    modifier: Modifier = Modifier,
    onCardClick: () -> Unit,
    onThumbnailClick: (String) -> Unit,
) {
    Card(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 4.dp),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 8.dp
        ),
        colors = CardDefaults.cardColors(
            containerColor = Color.White,
        ),
        onClick = onCardClick,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            AsyncImage(
                modifier = Modifier
                    .width(128.dp)
                    .height(128.dp)
                    .clickable { onThumbnailClick(mealThumbnailUrl) },
                model = mealThumbnailUrl,
                contentDescription = "meal thumbnail",
                placeholder = ColorPainter(AppColor.mist40),
                error = ColorPainter(AppColor.mist40),
            )
            Column(
                modifier = Modifier
                    .padding(8.dp)
            ) {
                Text(
                    modifier = Modifier.padding(
                        end = 16.dp,
                    ),
                    text = mealName,
                    color = Color.Black,
                    style = AppTypography.contentMediumBold,
                )
                Text(
                    modifier = Modifier.padding(
                        top = 8.dp,
                    ),
                    text = stringResource(id = R.string.meal_item_category_prefix, mealCategory),
                    color = AppColor.mist80,
                    style = AppTypography.contentMediumRegular,
                )
                Text(
                    modifier = Modifier.padding(
                        top = 8.dp,
                    ),
                    text = stringResource(id = R.string.meal_item_origin_prefix, mealOrigin),
                    color = AppColor.mist80,
                    style = AppTypography.contentMediumRegular,
                )
                Text(
                    modifier = Modifier.padding(
                        top = 8.dp,
                    ),
                    text = mealTags,
                    color = AppColor.mist80,
                    style = AppTypography.contentMediumRegular,
                )
            }
        }
    }
}