package android.venture.recipe.feature.recipelist.list.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.ViewAction
import android.venture.recipe.core.base.ViewState
import android.venture.recipe.data.model.Meal

data class RecipeListViewState(
    val isLoading: Boolean = false,
    val mealList: List<Meal> = emptyList(),
    val errorMessage: String = "",
    val isSuccessLogout: Boolean = false,
) : ViewState

sealed interface RecipeListViewAction : ViewAction {
    data class OnQuerySubmitted(val firstLetter: String) : RecipeListViewAction

    object OnLogoutClick: RecipeListViewAction
}

sealed interface RecipeListActionResult : ActionResult {
    object ShowLoading : RecipeListActionResult
}