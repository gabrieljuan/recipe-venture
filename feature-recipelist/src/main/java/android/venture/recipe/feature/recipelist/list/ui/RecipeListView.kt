@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package android.venture.recipe.feature.recipelist.list.ui

import android.venture.recipe.data.model.Meal
import android.venture.recipe.feature.recipelist.R
import android.venture.recipe.feature.recipelist.list.viewmodel.RecipeListViewAction
import android.venture.recipe.feature.recipelist.list.viewmodel.RecipeListViewModel
import android.venture.recipe.feature.recipelist.list.viewmodel.RecipeListViewState
import android.venture.recipe.navigation.LoginNavigationRoute.LOGIN_NAVGRAPH_ROUTE
import android.venture.recipe.navigation.RecipeListNavigationRoute.RECIPE_DETAIL_ID_ARGS
import android.venture.recipe.navigation.RecipeListNavigationRoute.RECIPE_DETAIL_ROUTE
import android.venture.recipe.navigation.navigateTo
import android.venture.recipe.theme.AppColor
import android.venture.recipe.theme.AppTypography
import android.venture.recipe.widget.AppBar
import android.venture.recipe.widget.CustomSnackbarHost
import android.venture.recipe.widget.CustomSnackbarVisuals
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import net.engawapg.lib.zoomable.rememberZoomState
import net.engawapg.lib.zoomable.zoomable

@Composable
fun RecipeListRoute(
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel: RecipeListViewModel = hiltViewModel()
    val viewState: RecipeListViewState by viewModel.viewState.observeAsState(initial = RecipeListViewState())

    var zoomableImageUrl by remember { mutableStateOf("") }

    LaunchedEffect(viewState.isSuccessLogout) {
        if (viewState.isSuccessLogout) {
            navController.popBackStack()
            navController.navigate(LOGIN_NAVGRAPH_ROUTE)
        }
    }

    Box(
        modifier = modifier
    ) {
        MealListScreen(
            mealList = viewState.mealList,
            errorMessage = viewState.errorMessage,
            isLoading = viewState.isLoading,
            onMealClick = {
                navController.navigateTo(
                    route = RECIPE_DETAIL_ROUTE,
                    args = bundleOf(
                        RECIPE_DETAIL_ID_ARGS to it.id
                    )
                )
            },
            onBackClick = { navController.navigateUp() },
            onThumbnailClick = { zoomableImageUrl = it },
            onSearchValueChange = {
                if (it.isNotBlank()) {
                    viewModel.onAction(RecipeListViewAction.OnQuerySubmitted(it))
                }
            },
            onLogoutClick = {
                viewModel.onAction(RecipeListViewAction.OnLogoutClick)
            }
        )
        if (zoomableImageUrl.isNotEmpty()) {
            ZoomableImagePopup(
                zoomableImageUrl = zoomableImageUrl,
                onCloseClick = { zoomableImageUrl = "" },
            )
        }

    }

}

@Composable
private fun MealListScreen(
    mealList: List<Meal>,
    errorMessage: String,
    isLoading: Boolean,
    modifier: Modifier = Modifier,
    onMealClick: (Meal) -> Unit,
    onBackClick: () -> Unit,
    onSearchValueChange: (String) -> Unit,
    onThumbnailClick: (String) -> Unit,
    onLogoutClick: () -> Unit,
    listState: LazyListState = rememberLazyListState(),
) {
    val snackBarHostState = remember { SnackbarHostState() }

    LaunchedEffect(errorMessage) {
        if (errorMessage.isNotEmpty()) {
            snackBarHostState.showSnackbar(
                CustomSnackbarVisuals(
                    message = errorMessage,
                    isError = true,
                )
            )
        }
    }

    var textFieldValue by remember {
        mutableStateOf("")
    }

    val inputFilter = remember { Regex("[a-zA-z\\s]*$") }

    Scaffold(
        modifier = modifier,
        containerColor = AppColor.mist20,
        topBar = {
            AppBar(
                title = stringResource(id = R.string.meal_list_title),
                onClickNavigationIcon = onBackClick,
                actions = {
                    Text(
                        modifier = Modifier.clickable(onClick = onLogoutClick),
                        text = stringResource(id = R.string.meal_list_logout_action_menu),
                        color = Color.White,
                        style = AppTypography.contentMediumRegular,
                    )
                }
            )
        },
        snackbarHost = { CustomSnackbarHost(hostState = snackBarHostState) }
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            TextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        vertical = 8.dp,
                        horizontal = 16.dp
                    ),
                value = textFieldValue,
                onValueChange = { value ->
                    if (value.length <= 1 && value.matches(inputFilter)) {
                        textFieldValue = value
                        onSearchValueChange(value)
                    }
                },
                label = {
                    Text(
                        text = stringResource(id = R.string.meal_list_search_field_Label),
                        color = AppColor.mist80,
                        style = AppTypography.contentSmallRegular,
                    )
                }
            )
            LazyColumn(
                modifier = Modifier
                    .padding(
                        top = 80.dp,
                    )
                    .fillMaxWidth(),
                state = listState,
            ) {
                items(mealList) { meal ->
                    MealCardItem(
                        mealName = meal.name,
                        mealCategory = meal.category,
                        mealOrigin = meal.area,
                        mealTags = meal.tags.orEmpty(),
                        mealThumbnailUrl = meal.thumbnailUrl,
                        onCardClick = { onMealClick(meal) },
                        onThumbnailClick = onThumbnailClick,
                    )
                }
            }
            if (isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }
        }
    }
}

@Composable
private fun ZoomableImagePopup(
    zoomableImageUrl: String,
    onCloseClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Box(
        modifier = modifier
            .background(color = Color.DarkGray.copy(alpha = 0.8f))
            .fillMaxSize()
    ) {
        AsyncImage(
            modifier = Modifier
                .fillMaxSize()
                .padding(32.dp)
                .align(Alignment.Center)
                .zoomable(rememberZoomState()),
            model = zoomableImageUrl,
            contentDescription = "zoomable image",
        )
        IconButton(
            modifier = Modifier
                .padding(8.dp)
                .align(Alignment.TopEnd),
            onClick = onCloseClick,
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_close_white_24),
                contentDescription = "close button",
                tint = Color.White,
            )
        }
    }

    BackHandler(onBack = onCloseClick)
}