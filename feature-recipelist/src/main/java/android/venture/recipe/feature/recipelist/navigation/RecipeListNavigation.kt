package android.venture.recipe.feature.recipelist.navigation

import android.venture.recipe.feature.recipelist.detail.ui.RecipeDetailRoute
import android.venture.recipe.feature.recipelist.list.ui.RecipeListRoute
import android.venture.recipe.navigation.RecipeListNavigationRoute
import android.venture.recipe.navigation.RecipeListNavigationRoute.RECIPE_DETAIL_ID_ARGS
import android.venture.recipe.navigation.RecipeListNavigationRoute.RECIPE_DETAIL_ROUTE
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.navigation

fun NavGraphBuilder.recipeListNavGraph(navController: NavController) {
    navigation(
        startDestination = RecipeListNavigationRoute.RECIPE_LIST_ROUTE,
        route = RecipeListNavigationRoute.RECIPE_LIST_NAVGRAPH_ROUTE,
    ) {
        composable(route = RecipeListNavigationRoute.RECIPE_LIST_ROUTE) {
            RecipeListRoute(navController = navController)
        }
        composable(route = RECIPE_DETAIL_ROUTE) { backStackEntry ->
            val mealId = backStackEntry.arguments?.getString(RECIPE_DETAIL_ID_ARGS, "").orEmpty()
            RecipeDetailRoute(
                mealId = mealId,
                navController = navController,
            )
        }
    }
}