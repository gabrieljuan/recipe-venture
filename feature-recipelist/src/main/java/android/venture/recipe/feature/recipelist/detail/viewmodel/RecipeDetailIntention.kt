package android.venture.recipe.feature.recipelist.detail.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.ViewAction
import android.venture.recipe.core.base.ViewState
import android.venture.recipe.data.model.Meal

data class RecipeDetailViewState(
    val meal: Meal? = null,
    val isLoading: Boolean = true,
    val errorMessage: String = ""
) : ViewState

sealed interface RecipeDetailViewAction : ViewAction {
    data class OnInit(val mealId: String) : RecipeDetailViewAction
}

sealed interface RecipeDetailActionResult : ActionResult {
    object ShowLoading : RecipeDetailActionResult
}