@file:OptIn(ExperimentalMaterial3Api::class)

package android.venture.recipe.feature.recipelist.detail.ui

import android.venture.recipe.data.model.Meal
import android.venture.recipe.feature.recipelist.detail.viewmodel.RecipeDetailViewAction
import android.venture.recipe.feature.recipelist.detail.viewmodel.RecipeDetailViewModel
import android.venture.recipe.feature.recipelist.detail.viewmodel.RecipeDetailViewState
import android.venture.recipe.theme.AppColor
import android.venture.recipe.theme.AppTypography
import android.venture.recipe.widget.AppBar
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import net.engawapg.lib.zoomable.rememberZoomState
import net.engawapg.lib.zoomable.zoomable

@Composable
fun RecipeDetailRoute(
    mealId: String,
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel: RecipeDetailViewModel = hiltViewModel()
    val viewState: RecipeDetailViewState by viewModel.viewState.observeAsState(RecipeDetailViewState())

    LaunchedEffect(Unit) {
        viewModel.onAction(RecipeDetailViewAction.OnInit(mealId))
    }

    RecipeDetailScreen(
        modifier = modifier,
        meal = viewState.meal,
        isLoading = viewState.isLoading,
        onBackClick = { navController.navigateUp() }
    )
}

@Composable
private fun RecipeDetailScreen(
    meal: Meal?,
    isLoading: Boolean,
    onBackClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Scaffold(
        modifier = modifier,
        containerColor = AppColor.mist20,
        topBar = {
            AppBar(
                title = meal?.name.orEmpty(),
                navigationIcon = android.venture.recipe.R.drawable.ic_back,
                onClickNavigationIcon = onBackClick,
            )
        }
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            if (meal != null) {
                LazyColumn(
                    modifier = Modifier
                        .fillMaxSize(),
                    state = rememberLazyListState(),
                ) {
                    item {
                        AsyncImage(
                            modifier = Modifier
                                .fillMaxWidth()
                                .aspectRatio(1f)
                                .zoomable(rememberZoomState()),
                            model = meal.thumbnailUrl,
                            contentDescription = "muvi backdrop",
                        )
                    }
                    item {
                        Text(
                            modifier = Modifier.padding(
                                horizontal = 16.dp,
                                vertical = 8.dp,
                            ),
                            text = meal.name,
                            style = AppTypography.titleMediumBold,
                            color = Color.Black,
                        )
                    }
                    item {
                        Text(
                            modifier = Modifier.padding(
                                horizontal = 16.dp,
                            ),
                            text = meal.category,
                            style = AppTypography.contentMediumRegular,
                            color = AppColor.mist80,
                        )
                    }
                    item {
                        Text(
                            modifier = Modifier.padding(
                                start = 16.dp,
                                end = 16.dp,
                                top = 8.dp,
                            ),
                            text = meal.area,
                            style = AppTypography.contentMediumRegular,
                            color = AppColor.mist80,
                        )
                    }
                    item {
                        Text(
                            modifier = Modifier.padding(
                                start = 16.dp,
                                end = 16.dp,
                                top = 8.dp,
                            ),
                            text = meal.instructions,
                            style = AppTypography.contentSmallRegular,
                            color = AppColor.mist100,
                        )
                    }
                }
            }
            if (isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            }
        }

    }
}