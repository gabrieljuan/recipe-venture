package android.venture.recipe.feature.recipelist.detail.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseViewModel
import android.venture.recipe.domain.usecase.FetchMealByIdActionResult
import android.venture.recipe.domain.usecase.FetchMealByIdUseCase
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RecipeDetailViewModel @Inject constructor(
    private val fetchMealByIdUseCase: FetchMealByIdUseCase
) : BaseViewModel<RecipeDetailViewState, RecipeDetailViewAction>(RecipeDetailViewState()) {
    override fun renderViewState(result: ActionResult?): RecipeDetailViewState =
        when (result) {
            RecipeDetailActionResult.ShowLoading -> getCurrentViewState().copy(
                isLoading = true,
            )

            is FetchMealByIdActionResult -> result.mapFetchMealByIdResult()
            else -> getCurrentViewState()
        }

    override fun handleAction(action: RecipeDetailViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is RecipeDetailViewAction.OnInit -> {
                    emit(RecipeDetailActionResult.ShowLoading)
                    emit(fetchMealByIdUseCase.getResult(FetchMealByIdUseCase.Params(action.mealId)))
                }
            }
        }

    private fun FetchMealByIdActionResult.mapFetchMealByIdResult(): RecipeDetailViewState =
        when (this) {
            is FetchMealByIdActionResult.Success -> getCurrentViewState().copy(
                meal = meal,
                isLoading = false,
            )

            else -> getCurrentViewState().copy(
                isLoading = false,
                errorMessage = "Failed to get recipe detail"
            )
        }

}