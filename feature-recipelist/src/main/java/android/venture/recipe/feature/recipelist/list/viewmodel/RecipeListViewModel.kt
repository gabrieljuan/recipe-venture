package android.venture.recipe.feature.recipelist.list.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseViewModel
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterActionResult
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterUseCase
import android.venture.recipe.domain.usecase.LogoutUseActionResult
import android.venture.recipe.domain.usecase.LogoutUseCase
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RecipeListViewModel @Inject constructor(
    private val fetchMealByFirstLetterUseCase: FetchMealByFirstLetterUseCase,
    private val logoutUseCase: LogoutUseCase,
) : BaseViewModel<RecipeListViewState, RecipeListViewAction>(RecipeListViewState()) {

    private var lastQuery: String = ""
    override fun renderViewState(result: ActionResult?): RecipeListViewState =
        when (result) {
            RecipeListActionResult.ShowLoading -> getCurrentViewState().copy(isLoading = true)
            is FetchMealByFirstLetterActionResult -> result.mapFetchMealByFirstLetterResult()
            is LogoutUseActionResult -> result.mapLogoutResult()
            else -> getCurrentViewState()
        }

    override fun handleAction(action: RecipeListViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is RecipeListViewAction.OnQuerySubmitted -> {
                    if (action.firstLetter == lastQuery) return@liveData

                    lastQuery = action.firstLetter
                    emit(RecipeListActionResult.ShowLoading)
                    emit(
                        fetchMealByFirstLetterUseCase.getResult(
                            FetchMealByFirstLetterUseCase.Params(action.firstLetter[0])
                        )
                    )
                }

                RecipeListViewAction.OnLogoutClick -> emit(logoutUseCase.getResult())
            }
        }

    private fun FetchMealByFirstLetterActionResult.mapFetchMealByFirstLetterResult(): RecipeListViewState =
        when (this) {
            is FetchMealByFirstLetterActionResult.Success -> {
                getCurrentViewState().copy(
                    mealList = mealList,
                    isLoading = false,
                )
            }

            is FetchMealByFirstLetterActionResult.Failed -> getCurrentViewState().copy(
                errorMessage = "Failed to fetch meal list",
                isLoading = false,
            )

            else -> getCurrentViewState().copy(isLoading = false)
        }

    private fun LogoutUseActionResult.mapLogoutResult(): RecipeListViewState =
        when (this) {
            LogoutUseActionResult.Success -> getCurrentViewState().copy(
                isSuccessLogout = true,
            )

            is LogoutUseActionResult.Failed -> getCurrentViewState().copy(
                errorMessage = throwable.message ?: "Something went wrong"
            )
        }
}