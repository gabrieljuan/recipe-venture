package android.venture.recipe.feature.recipelist.list

import android.venture.recipe.data.model.Meal
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterActionResult
import android.venture.recipe.domain.usecase.FetchMealByFirstLetterUseCase
import android.venture.recipe.domain.usecase.LogoutUseActionResult
import android.venture.recipe.domain.usecase.LogoutUseCase
import android.venture.recipe.feature.recipelist.CoroutineTestRule
import android.venture.recipe.feature.recipelist.list.viewmodel.RecipeListViewAction
import android.venture.recipe.feature.recipelist.list.viewmodel.RecipeListViewModel
import android.venture.recipe.feature.recipelist.list.viewmodel.RecipeListViewState
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

@ExperimentalCoroutinesApi
class RecipeListViewModelTest {

    @get:Rule
    val liveDataRule: TestRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val rule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @Mock
    private lateinit var observer: Observer<RecipeListViewState>

    @Mock
    private lateinit var fetchMealByFirstLetterUseCase: FetchMealByFirstLetterUseCase

    @Mock
    private lateinit var logoutUseCase: LogoutUseCase

    @Mock
    private lateinit var meal: Meal

    private lateinit var viewModel: RecipeListViewModel

    private lateinit var mock: AutoCloseable

    @Before
    fun setup() {
        mock = MockitoAnnotations.openMocks(this)
        viewModel = RecipeListViewModel(fetchMealByFirstLetterUseCase, logoutUseCase)
        viewModel.viewState.observeForever(observer)
    }

    @After
    fun teardown() {
        mock.close()
        viewModel.viewState.removeObserver(observer)
    }

    @Test
    fun `check when user input query and network request return success`() = runTest {
        // Given
        Mockito.`when`(fetchMealByFirstLetterUseCase.getResult(Mockito.any()))
            .thenReturn(FetchMealByFirstLetterActionResult.Success(listOf(meal)))

        // When
        viewModel.onAction(RecipeListViewAction.OnQuerySubmitted("A"))

        // Then
        val currentState = viewModel.viewState.value!!
        Assert.assertTrue(currentState.mealList.size == 1)
        Assert.assertFalse(currentState.isLoading)
        Assert.assertEquals("", currentState.errorMessage)
        Assert.assertFalse(currentState.isSuccessLogout)
    }

    @Test
    fun `check when user input query and network request return error`() = runTest {
        // Given
        Mockito.`when`(fetchMealByFirstLetterUseCase.getResult(Mockito.any()))
            .thenReturn(FetchMealByFirstLetterActionResult.Failed)

        // When
        viewModel.onAction(RecipeListViewAction.OnQuerySubmitted("A"))

        // Then
        val currentState = viewModel.viewState.value!!
        Assert.assertTrue(currentState.mealList.isEmpty())
        Assert.assertFalse(currentState.isLoading)
        Assert.assertEquals("Failed to fetch meal list", currentState.errorMessage)
        Assert.assertFalse(currentState.isSuccessLogout)
    }

    @Test
    fun `check when user input query and network request return empty`() = runTest {
        // Given
        Mockito.`when`(fetchMealByFirstLetterUseCase.getResult(Mockito.any()))
            .thenReturn(FetchMealByFirstLetterActionResult.Empty)

        // When
        viewModel.onAction(RecipeListViewAction.OnQuerySubmitted("A"))

        // Then
        val currentState = viewModel.viewState.value!!
        Assert.assertTrue(currentState.mealList.isEmpty())
        Assert.assertFalse(currentState.isLoading)
        Assert.assertEquals("", currentState.errorMessage)
        Assert.assertFalse(currentState.isSuccessLogout)
    }

    @Test
    fun `check when user logout success`() = runTest {
        // Given
        Mockito.`when`(logoutUseCase.getResult(Mockito.any()))
            .thenReturn(LogoutUseActionResult.Success)

        // When
        viewModel.onAction(RecipeListViewAction.OnLogoutClick)

        // Then
        val currentState = viewModel.viewState.value!!
        Assert.assertTrue(currentState.mealList.isEmpty())
        Assert.assertFalse(currentState.isLoading)
        Assert.assertEquals("", currentState.errorMessage)
        Assert.assertTrue(currentState.isSuccessLogout)
    }

    @Test
    fun `check when user logout failed`() = runTest {
        // Given
        Mockito.`when`(logoutUseCase.getResult(Mockito.any()))
            .thenReturn(LogoutUseActionResult.Failed(Throwable("Logout failed")))

        // When
        viewModel.onAction(RecipeListViewAction.OnLogoutClick)

        // Then
        val currentState = viewModel.viewState.value!!
        Assert.assertTrue(currentState.mealList.isEmpty())
        Assert.assertFalse(currentState.isLoading)
        Assert.assertEquals("Logout failed", currentState.errorMessage)
        Assert.assertFalse(currentState.isSuccessLogout)
    }
}