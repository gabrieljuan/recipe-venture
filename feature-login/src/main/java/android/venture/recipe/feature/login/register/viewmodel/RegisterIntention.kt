package android.venture.recipe.feature.login.register.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.ViewAction
import android.venture.recipe.core.base.ViewState

data class RegisterViewState(
    val isRegisterSuccess: Boolean = false,
    val isLoading: Boolean = false,
    val errorMessage: String = "",
) : ViewState

sealed interface RegisterViewAction : ViewAction {
    data class Register(val username: String, val password: String) : RegisterViewAction
}

sealed interface RegisterActionResult : ActionResult {
    object ShowLoading : RegisterActionResult
}