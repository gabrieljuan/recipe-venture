package android.venture.recipe.feature.login.landing.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.ViewAction
import android.venture.recipe.core.base.ViewState

data class LandingViewState(
    val isLoginSuccess: Boolean = false,
    val isFullScreenLoading: Boolean = true,
    val isButtonLoading: Boolean = false,
    val errorMessage: String = "",
) : ViewState

sealed interface LandingViewAction : ViewAction {
    object LoginWithToken : LandingViewAction

    data class Login(val username: String, val password: String) : LandingViewAction
}

sealed interface LandingActionResult : ActionResult {
    data class ShowLoading(val isShown: Boolean) : LandingActionResult
    object ShowButtonLoading: LandingActionResult
}