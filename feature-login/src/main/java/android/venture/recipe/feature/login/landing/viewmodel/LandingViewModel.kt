package android.venture.recipe.feature.login.landing.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseViewModel
import android.venture.recipe.domain.usecase.LoginUseCase
import android.venture.recipe.domain.usecase.LoginUseCaseActionResult
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LandingViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase
) : BaseViewModel<LandingViewState, LandingViewAction>(LandingViewState()) {
    override fun renderViewState(result: ActionResult?): LandingViewState =
        when (result) {
            is LandingActionResult.ShowLoading -> getCurrentViewState().copy(
                isFullScreenLoading = result.isShown,
            )

            is LoginUseCaseActionResult -> result.mapLoginResult()
            else -> getCurrentViewState()
        }

    override fun handleAction(action: LandingViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is LandingViewAction.LoginWithToken -> {
                    val result = loginUseCase.getResult()
                    if (result is LoginUseCaseActionResult.Success) {
                        emit(result)
                    } else {
                        emit(LandingActionResult.ShowLoading(false))
                    }
                }

                is LandingViewAction.Login -> {
                    emit(LandingActionResult.ShowButtonLoading)
                    emit(
                        loginUseCase.getResult(
                            LoginUseCase.Params(
                                action.username,
                                action.password
                            )
                        )
                    )
                }
            }
        }

    private fun LoginUseCaseActionResult.mapLoginResult(): LandingViewState =
        when (this) {
            is LoginUseCaseActionResult.Success -> getCurrentViewState().copy(
                isLoginSuccess = true,
                isButtonLoading = false,
            )

            is LoginUseCaseActionResult.Failed -> getCurrentViewState().copy(
                isLoginSuccess = false,
                isButtonLoading = false,
                errorMessage = throwable.message ?: "Something went wrong"
            )
        }
}