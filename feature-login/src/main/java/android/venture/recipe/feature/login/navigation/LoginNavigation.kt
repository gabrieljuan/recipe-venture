package android.venture.recipe.feature.login.navigation

import android.venture.recipe.feature.login.landing.ui.LandingRoute
import android.venture.recipe.feature.login.register.ui.RegisterRoute
import android.venture.recipe.navigation.LoginNavigationRoute
import android.venture.recipe.navigation.LoginNavigationRoute.LANDING_ROUTE
import android.venture.recipe.navigation.LoginNavigationRoute.LOGIN_NAVGRAPH_ROUTE
import android.venture.recipe.navigation.LoginNavigationRoute.REGISTER_ROUTE
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.navigation

fun NavGraphBuilder.loginNavGraph(navController: NavController) {
    navigation(
        startDestination = LANDING_ROUTE,
        route = LOGIN_NAVGRAPH_ROUTE,
    ) {
        composable(route = LANDING_ROUTE) {
            LandingRoute(navController = navController)
        }
        composable(route = REGISTER_ROUTE) {
            RegisterRoute(navController = navController)
        }
    }
}