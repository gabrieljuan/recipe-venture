@file:OptIn(ExperimentalMaterial3Api::class)

package android.venture.recipe.feature.login.landing.ui

import android.venture.recipe.feature.login.R
import android.venture.recipe.feature.login.landing.viewmodel.LandingViewAction
import android.venture.recipe.feature.login.landing.viewmodel.LandingViewModel
import android.venture.recipe.feature.login.landing.viewmodel.LandingViewState
import android.venture.recipe.navigation.LoginNavigationRoute.REGISTER_ROUTE
import android.venture.recipe.navigation.RecipeListNavigationRoute.RECIPE_LIST_NAVGRAPH_ROUTE
import android.venture.recipe.theme.AppColor
import android.venture.recipe.theme.AppTypography
import android.venture.recipe.widget.CustomSnackbarHost
import android.venture.recipe.widget.CustomSnackbarVisuals
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController

@Composable
fun LandingRoute(
    navController: NavController,
    modifier: Modifier = Modifier,
) {

    val viewModel: LandingViewModel = hiltViewModel()
    val viewState: LandingViewState by viewModel.viewState.observeAsState(LandingViewState())

    LaunchedEffect(Unit) {
        viewModel.onAction(LandingViewAction.LoginWithToken)
    }

    LaunchedEffect(viewState.isLoginSuccess) {
        if (viewState.isLoginSuccess) {
            navController.popBackStack()
            navController.navigate(RECIPE_LIST_NAVGRAPH_ROUTE)
        }
    }

    LandingScreen(
        modifier = modifier,
        isFullscreenLoading = viewState.isFullScreenLoading,
        isButtonLoading = viewState.isButtonLoading,
        errorMessage = viewState.errorMessage,
        onLoginButtonClick = { username, password ->
            viewModel.onAction(LandingViewAction.Login(username, password))
        },
        onRegisterButtonClick = {
            navController.navigate(REGISTER_ROUTE)
        },
    )
}

@Composable
private fun LandingScreen(
    isFullscreenLoading: Boolean,
    isButtonLoading: Boolean,
    errorMessage: String,
    onLoginButtonClick: (String, String) -> Unit,
    onRegisterButtonClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val snackBarHostState = remember { SnackbarHostState() }
    var usernameValue by remember {
        mutableStateOf("")
    }
    var passwordValue by remember {
        mutableStateOf("")
    }
    var isPasswordVisible by remember {
        mutableStateOf(false)
    }

    LaunchedEffect(errorMessage) {
        if (errorMessage.isNotEmpty()) {
            snackBarHostState.showSnackbar(
                CustomSnackbarVisuals(
                    message = errorMessage,
                    isError = true,
                )
            )
        }
    }

    Scaffold(
        modifier = modifier,
        containerColor = AppColor.mist20,
        snackbarHost = { CustomSnackbarHost(hostState = snackBarHostState) }
    ) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
        ) {
            if (isFullscreenLoading) {
                CircularProgressIndicator(
                    modifier = Modifier
                        .align(Alignment.Center)
                )
            } else {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .verticalScroll(rememberScrollState()),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    Image(
                        modifier = Modifier.weight(1f),
                        painter = painterResource(id = R.drawable.art_landing),
                        contentDescription = "landing art"
                    )
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                vertical = 8.dp,
                                horizontal = 16.dp
                            ),
                        value = usernameValue,
                        onValueChange = { value ->
                            if (value.length <= 20) {
                                usernameValue = value
                            }
                        },
                        label = {
                            Text(
                                text = stringResource(id = R.string.landing_username_label),
                                color = AppColor.mist80,
                                style = AppTypography.contentSmallRegular,
                            )
                        }
                    )
                    TextField(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                vertical = 8.dp,
                                horizontal = 16.dp
                            ),
                        value = passwordValue,
                        onValueChange = { value ->
                            if (value.length <= 20) {
                                passwordValue = value
                            }
                        },
                        label = {
                            Text(
                                text = stringResource(id = R.string.landing_password_label),
                                color = AppColor.mist80,
                                style = AppTypography.contentSmallRegular,
                            )
                        },
                        trailingIcon = {
                            val iconRes = if (isPasswordVisible)
                                R.drawable.ic_visibility_on
                            else
                                R.drawable.ic_visibility_off
                            val description =
                                if (isPasswordVisible) "Hide password" else "Show password"

                            IconButton(onClick = { isPasswordVisible = !isPasswordVisible }) {
                                Icon(
                                    painter = painterResource(id = iconRes),
                                    contentDescription = description
                                )
                            }
                        },
                        visualTransformation = if (isPasswordVisible) VisualTransformation.None else PasswordVisualTransformation()
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Button(
                        contentPadding = PaddingValues(horizontal = 40.dp, vertical = 12.dp),
                        onClick = {
                            onLoginButtonClick(usernameValue, passwordValue)
                        }
                    ) {
                        if (isButtonLoading) {
                            CircularProgressIndicator()
                        } else {
                            Text(
                                text = stringResource(id = R.string.landing_login_button_label),
                                color = Color.White,
                                style = AppTypography.contentSmallRegular,
                            )
                        }
                    }
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        modifier = Modifier.clickable(onClick = onRegisterButtonClick),
                        text = stringResource(id = R.string.landing_register_text),
                        color = AppColor.mist80,
                        style = AppTypography.contentMediumBold,
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                }
            }
        }
    }
}