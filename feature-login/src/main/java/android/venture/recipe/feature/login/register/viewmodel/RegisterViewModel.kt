package android.venture.recipe.feature.login.register.viewmodel

import android.venture.recipe.core.base.ActionResult
import android.venture.recipe.core.base.BaseViewModel
import android.venture.recipe.domain.usecase.RegisterUseCase
import android.venture.recipe.domain.usecase.RegisterUseCaseActionResult
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val registerUseCase: RegisterUseCase
) : BaseViewModel<RegisterViewState, RegisterViewAction>(RegisterViewState()) {

    override fun renderViewState(result: ActionResult?): RegisterViewState =
        when (result) {
            RegisterActionResult.ShowLoading -> getCurrentViewState()
                .copy(isLoading = true)

            is RegisterUseCaseActionResult -> result.mapRegisterActionResult()
            else -> getCurrentViewState()
        }

    override fun handleAction(action: RegisterViewAction): LiveData<ActionResult> =
        liveData(viewModelDispatcher) {
            when (action) {
                is RegisterViewAction.Register -> {
                    emit(RegisterActionResult.ShowLoading)
                    emit(
                        registerUseCase.getResult(
                            RegisterUseCase.Params(
                                action.username,
                                action.password
                            )
                        )
                    )
                }
            }
        }

    private fun RegisterUseCaseActionResult.mapRegisterActionResult(): RegisterViewState =
        when (this) {
            RegisterUseCaseActionResult.Success -> getCurrentViewState().copy(
                isLoading = false,
                isRegisterSuccess = true,
            )

            is RegisterUseCaseActionResult.Failed -> getCurrentViewState().copy(
                isLoading = false,
                errorMessage = throwable.message ?: "Something went wrong",
                isRegisterSuccess = false,
            )
        }
}