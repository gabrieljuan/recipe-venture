@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterial3Api::class)

package android.venture.recipe.feature.login.register.ui

import android.venture.recipe.feature.login.R
import android.venture.recipe.feature.login.register.viewmodel.RegisterViewAction
import android.venture.recipe.feature.login.register.viewmodel.RegisterViewModel
import android.venture.recipe.feature.login.register.viewmodel.RegisterViewState
import android.venture.recipe.navigation.RecipeListNavigationRoute.RECIPE_LIST_NAVGRAPH_ROUTE
import android.venture.recipe.theme.AppColor
import android.venture.recipe.theme.AppTypography
import android.venture.recipe.widget.AppBar
import android.venture.recipe.widget.CustomSnackbarHost
import android.venture.recipe.widget.CustomSnackbarVisuals
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController

@Composable
fun RegisterRoute(
    navController: NavController,
    modifier: Modifier = Modifier,
) {

    val viewModel: RegisterViewModel = hiltViewModel()
    val viewState: RegisterViewState by viewModel.viewState.observeAsState(RegisterViewState())

    LaunchedEffect(viewState.isRegisterSuccess) {
        if (viewState.isRegisterSuccess) {
            navController.popBackStack()
            navController.navigate(
                route = RECIPE_LIST_NAVGRAPH_ROUTE,
            )
        }
    }

    RegisterScreen(
        modifier = modifier,
        isLoading = viewState.isLoading,
        errorMessage = viewState.errorMessage,
        onRegisterButtonClick = { username, password ->
            viewModel.onAction(RegisterViewAction.Register(username, password))
        },
        onBackClick = { navController.navigateUp() },
    )
}

@Composable
fun RegisterScreen(
    isLoading: Boolean,
    errorMessage: String,
    onRegisterButtonClick: (String, String) -> Unit,
    onBackClick: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val snackBarHostState = remember { SnackbarHostState() }
    var usernameValue by remember {
        mutableStateOf("")
    }
    var passwordValue by remember {
        mutableStateOf("")
    }
    var isPasswordVisible by remember {
        mutableStateOf(false)
    }

    LaunchedEffect(errorMessage) {
        if (errorMessage.isNotEmpty()) {
            snackBarHostState.showSnackbar(
                CustomSnackbarVisuals(
                    message = errorMessage,
                    isError = true,
                )
            )
        }
    }

    Scaffold(
        modifier = modifier,
        containerColor = AppColor.mist20,
        topBar = {
            AppBar(
                title = stringResource(id = R.string.register_title),
                onClickNavigationIcon = onBackClick,
                navigationIcon = android.venture.recipe.R.drawable.ic_back
            )
        },
        snackbarHost = { CustomSnackbarHost(hostState = snackBarHostState) }
    ) {
        Column(
            modifier = Modifier.padding(it),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            TextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        vertical = 8.dp,
                        horizontal = 16.dp
                    ),
                value = usernameValue,
                onValueChange = { value ->
                    if (value.length <= 20) {
                        usernameValue = value
                    }
                },
                label = {
                    Text(
                        text = stringResource(id = R.string.landing_username_label),
                        color = AppColor.mist80,
                        style = AppTypography.contentSmallRegular,
                    )
                }
            )
            TextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        vertical = 8.dp,
                        horizontal = 16.dp
                    ),
                value = passwordValue,
                onValueChange = { value ->
                    if (value.length <= 20) {
                        passwordValue = value
                    }
                },
                label = {
                    Text(
                        text = stringResource(id = R.string.landing_password_label),
                        color = AppColor.mist80,
                        style = AppTypography.contentSmallRegular,
                    )
                },
                trailingIcon = {
                    val iconRes = if (isPasswordVisible)
                        R.drawable.ic_visibility_on
                    else
                        R.drawable.ic_visibility_off
                    val description =
                        if (isPasswordVisible) "Hide password" else "Show password"

                    IconButton(onClick = { isPasswordVisible = !isPasswordVisible }) {
                        Icon(
                            painter = painterResource(id = iconRes),
                            contentDescription = description
                        )
                    }
                },
                visualTransformation = if (isPasswordVisible) VisualTransformation.None else PasswordVisualTransformation()
            )
            Spacer(modifier = Modifier.height(8.dp))
            Button(
                contentPadding = PaddingValues(horizontal = 40.dp, vertical = 12.dp),
                onClick = {
                    onRegisterButtonClick(usernameValue, passwordValue)
                }
            ) {
                if (isLoading) {
                    CircularProgressIndicator()
                } else {
                    Text(
                        text = stringResource(id = R.string.register_button_label),
                        color = Color.White,
                        style = AppTypography.contentSmallRegular,
                    )
                }
            }
        }
    }
}