package android.venture.recipe.core.base

import android.util.Log
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel<VS : ViewState, VA : ViewAction>(initialState: VS) : ViewModel() {

    private val viewAction = MutableLiveData<VA>()
    val viewState: LiveData<VS> =
        viewAction.switchMap(::handleAction).map {
            renderViewState(it).updateCurrentViewState()
        }
    protected val viewModelDispatcher: CoroutineContext
        get() = viewModelScope.coroutineContext

    private var currentViewState: VS = initialState

    protected abstract fun renderViewState(result: ActionResult?): VS
    protected abstract fun handleAction(action: VA): LiveData<ActionResult>
    protected fun getCurrentViewState(): VS = currentViewState

    private fun VS.updateCurrentViewState(): VS = this.also {
        currentViewState = it
    }

    @MainThread
    fun onAction(action: VA) {
        viewAction.value = action
    }
}

interface ViewState

interface ViewAction

interface ActionResult